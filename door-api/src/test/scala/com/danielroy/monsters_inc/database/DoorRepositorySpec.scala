package com.danielroy.monsters_inc.database

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.database.DoorRepository.DoorNotFound
import com.danielroy.monsters_inc.{Child, Door, Fear, Timezone}
import io.getquill.context.ZioJdbc.DataSourceLayer
import zio.ZLayer
import zio.test.Assertion.{equalTo, fails}
import zio.test.{DefaultRunnableSpec, TestEnvironment, ZSpec, assert, assertTrue}

import javax.sql.DataSource

object DoorRepositorySpec extends DefaultRunnableSpec {

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val testChild =
    Child.makeZIO(1, "Daniel Roy", Seq(Fear.Darkness), 4, 5, (3, 9))
  private def testDoor = (id: Int, timezone: Int) =>
    testChild.flatMap(child => Door.makeZIO(id, (45.0000, 90.0000), timezone, child))

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("DoorRepository test spec")(
      test("Save and then get a door") {
        for {
          door <- testDoor(1, 1)
          _ <- DoorRepository.storeDoor(door)
          outDoor <- DoorRepository.retrieveDoorById(door.id)
          _ <- DoorRepository.deleteDoorById(door.id)
        } yield assertTrue(outDoor == door)
      },

      test("Save and then get several doors") {
        for {
          door1 <- testDoor(2, 1)
          door2 <- testDoor(3, 1)
          _ <- DoorRepository.storeManyDoors(Seq(door1, door2))
          outDoor1 <- DoorRepository.retrieveDoorById(door1.id)
          outDoor2 <- DoorRepository.retrieveDoorById(door2.id)
          _ <- DoorRepository.deleteDoorById(door1.id)
          _ <- DoorRepository.deleteDoorById(door2.id)
        } yield {
          assertTrue(outDoor1 == door1)
          assertTrue(outDoor2 == door2)
          assertTrue(outDoor1 != outDoor2)
        }
      },

      test("Fail when retrieving door by ID for non-existent ID") {
        for {
          id <- IntAbove0.fromIntZIO(9999)
          idLong <- DoorRepository.retrieveDoorById(id).exit
        } yield assert(idLong)(fails(equalTo(DoorNotFound(id))))
      },

      test("Get doors by timezone") {
        for {
          door4 <- testDoor(4, 2)
          door5 <- testDoor(5, 2)
          door6 <- testDoor(6, 3)
          _ <- DoorRepository.storeManyDoors(Seq(door4,door5,door6))
          timezone2 <- Timezone.fromZIO(2)
          timezone3 <- Timezone.fromZIO(3)
          doors2 <- DoorRepository.retrieveDoorsForTimezone(timezone2)
          doors3 <- DoorRepository.retrieveDoorsForTimezone(timezone3)
        } yield {
          assertTrue(doors2.length == 2)
          assertTrue(doors3.length == 1)
        }
      }
    ).provideCustomLayer(DataSource)
}
