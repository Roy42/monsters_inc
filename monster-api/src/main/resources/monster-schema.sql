CREATE TABLE IF NOT EXISTS MonsterDAO(
    monsterId int NOT NULL,
    name varchar(50),
    strengths varchar(500),
    endurance int,
    effectiveness int,
    PRIMARY KEY (monsterId)
)