package com.danielroy.monsters_inc.monsterApi

import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Greater
import eu.timepit.refined.types.string.NonEmptyString

case class MonsterApiConfig(
    host: NonEmptyString,
    port: Int Refined Greater[8000],
    threads: Int Refined Greater[0],
    monstersToSpawn: Int Refined Greater[10]
) {
  override def toString: String =
    s"MonsterApiConfig(host: ${host.value}, port: ${port.value}, threads: ${threads.value}, monstersToSpawn: ${monstersToSpawn.value})"
}

object MonsterApiConfig {

  import zio.config._
  import zio.config.refined._ // IntelliJ marks this as unused but it actually is
  import zio.config.magnolia.DeriveConfigDescriptor.descriptor

  val configDescriptor: ConfigDescriptor[MonsterApiConfig] =
    descriptor[MonsterApiConfig]
      .mapKey(toKebabCase)
      .mapKey(_.toLowerCase)

}
