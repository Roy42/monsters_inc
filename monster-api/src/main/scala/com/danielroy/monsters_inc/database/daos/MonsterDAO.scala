package com.danielroy.monsters_inc.database.daos

import com.danielroy.dnd_engine.JsonCoders.encodeRefined
import com.danielroy.monsters_inc.{FearStrength, Monster}
import com.danielroy.dnd_engine.JsonCoders._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, EncoderOps, JsonDecoder, JsonEncoder}
import zio.{Task, ZIO}

case class MonsterDAO(monsterId: Int, name: String, strengths: String, endurance: Int, effectiveness: Int)

object MonsterDAO {

  def monsterToDaoZIO(monster: Monster): Task[MonsterDAO] = ZIO.attempt {
    MonsterDAO(
      monsterId = monster.id.value,
      name = monster.name.value,
      strengths = monster.strengths.toJson,
      endurance = monster.endurance.value,
      effectiveness = monster.effectiveness.value
    )
  }

  implicit val monsterDaoEncoder: JsonEncoder[MonsterDAO] = DeriveJsonEncoder.gen
  implicit val monsterDaoDecoder: JsonDecoder[MonsterDAO] = DeriveJsonDecoder.gen

  implicit val fearStrengthEncoder: JsonEncoder[FearStrength] = DeriveJsonEncoder.gen
  implicit val fearStrengthDecoder: JsonDecoder[FearStrength] = DeriveJsonDecoder.gen
}
