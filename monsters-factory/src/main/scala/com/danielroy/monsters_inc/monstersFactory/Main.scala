package com.danielroy.monsters_inc.monstersFactory

import com.danielroy.monsters_inc.monstersFactory.clients.MonsterApiClient
import com.danielroy.monsters_inc.monstersFactory.powerPlant.PowerPlant
import com.danielroy.monsters_inc.monstersFactory.powerPlant.PowerPlant.PowerPlantError
import io.getquill.context.ZioJdbc.DataSourceLayer
import zhttp.service.{ChannelFactory, EventLoopGroup}
import zio._
import zio.config.ReadError
import zio.config.typesafe.TypesafeConfig

import javax.sql.DataSource

object Main extends ZIOAppDefault {

  sealed trait MonstersFactoryError {
    val msg: String
  }
  case class MonstersFactorySetupError(msg: String) extends MonstersFactoryError

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val makeChannelFactory: ZIO[MonstersFactoryConfig, Nothing, ZLayer[Any, Nothing, ChannelFactory with EventLoopGroup]] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
    } yield ChannelFactory.auto ++ EventLoopGroup.auto(config.clientThreads.value)

  private val makeConfigLayer =
    System
      .env("MONSTERS_FACTORY_CONF")
      .someOrFail("Please specify the Monsters Factory config file in the system environment variable MONSTERS_FACTORY_CONF")
      .foldZIO(
        _ => ZIO.fail(new IllegalArgumentException("Did not find the config file")),
        { configFilePath =>
          val file = new java.io.File(configFilePath)
          if (file.isFile) {
            ZIO.attempt(TypesafeConfig.fromHoconFile(file, MonstersFactoryConfig.configDescriptor))
          } else {
            ZIO.fail {
              val filesInDir = file.getParentFile.listFiles().map(_.getName).mkString(",")
              new IllegalArgumentException(s"Did not find the config file: $configFilePath. Did you mean any of these? $filesInDir")
            }
          }
        }
      )

  private val monstersFactory: ZIO[ZEnv, Object, Unit] = {
    for {
      configLayer    <- makeConfigLayer
      channelFactory <- makeChannelFactory.provideCustomLayer(configLayer)
      powerPlant <-
        PowerPlant.run
          .provide(
            Clock.live,
            Console.live,
            Random.live, // I shouldn't need to provide these 3
            DataSource,
            configLayer,
            channelFactory
          )
    } yield powerPlant
  }

  // TODO: if the MonstersFactory exits with an Error, it is being swallowed up somewhere
  override def run: ZIO[ZEnv with ZIOAppArgs, Any, Any] =
    monstersFactory
}
