package com.danielroy.monsters_inc

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.dnd_engine.JsonCoders._
import zio.IO
import zio.json.{DecoderOps, DeriveJsonDecoder, DeriveJsonEncoder, EncoderOps, JsonDecoder, JsonEncoder}

case class Door private (id: IntAbove0, geoLocation: GeoPoint, timezone: Timezone, child: Child)

object Door {
  def makeZIO(id: Int, geoLocation: (Double, Double), timezone: Int, child: Child): IO[String, Door] = {
    for {
      idParsed          <- IntAbove0.fromIntZIO(id)
      geoLocationParsed <- GeoPoint.fromZIO(geoLocation._1, geoLocation._2)
      timezoneParsed    <- Timezone.fromZIO(timezone)
    } yield Door(idParsed, geoLocationParsed, timezoneParsed, child)
  }

  implicit val encoder: JsonEncoder[Door] = DeriveJsonEncoder.gen[Door]
  implicit val decoder: JsonDecoder[Door] = DeriveJsonDecoder.gen[Door]
}
