package com.danielroy.monsters_inc.generator

import com.danielroy.dnd_engine.Ints.Int20Value
import com.danielroy.monsters_inc.monsters.getRandomFear
import com.danielroy.monsters_inc.{Fear, Monster}
import com.danielroy.randomData.RandomPerson._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Positive
import zio.{IO, Random, ZIO}

object MonsterGenerator {

  def generate(startingId: Int Refined Positive, num: Int Refined Positive): ZIO[Random, String, Seq[Monster]] =
    for {
      ids <- ZIO.succeed(startingId.value to num.value + 1)
      randomPeople <- generateRandomPeople(num.value).mapError(t => t.getMessage)
      monsters <- ZIO.foreach(randomPeople.zipWithIndex) { case (person, i) =>
        for {
          monsterName   <- personNameToMonsterName(person.name)
          fearStrengths <- genRandomFearStrengths
          endurance     <- Random.nextIntBetween(Int20Value.MIN, Int20Value.MAX)
          effectiveness <- Random.nextIntBetween(Int20Value.MIN, Int20Value.MAX)
          id <- ZIO.succeed(ids(i))
          monster       <- Monster.makeZIO(id, monsterName, fearStrengths, endurance, effectiveness)
        } yield monster
      }
    } yield monsters

  private def personNameToMonsterName(name: String): IO[String, String] =
    ZIO.attempt(scala.util.Random.shuffle(name.iterator).mkString).mapError(_.toString)

  private val genRandomFearStrengths =
    for {
      num <- Random.nextIntBetween(1, 10)
      fs <-
        ZIO.foreach(1 to num) {
          _ =>
            for {
              potency <- Random.nextIntBetween(Int20Value.MIN, Int20Value.MAX)
              fear <- getRandomFear
              fs <- Monster.makeFearStrengthZIO(fear, potency)
            } yield fs
        }
    } yield fs

}
