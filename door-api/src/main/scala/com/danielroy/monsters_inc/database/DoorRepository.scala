package com.danielroy.monsters_inc.database

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.database.daos.{ChildDAO, DoorDAO}
import com.danielroy.monsters_inc.doorApi.DoorApiError
import com.danielroy.monsters_inc.{Door, Timezone}
import io.getquill._
import zio.{IO, ZIO}

import javax.sql.DataSource

object DoorRepository {

  sealed trait DoorRepositoryError                      extends DoorApiError
  case class DoorSavingError(msg: String)               extends DoorRepositoryError
  case class DoorEncodingError(msg: String)             extends DoorRepositoryError
  case class DoorRetrievalError(msg: String)            extends DoorRepositoryError
  case class DoorDecodingError(msg: String)             extends DoorRepositoryError
  case class DoorNotFound(idNotFound: IntAbove0)        extends DoorRepositoryError
  case class DoorQueryError(msg: String)                extends DoorRepositoryError
  case class DoorDeletionError(msg: String)             extends DoorRepositoryError
  case class DoorRepositoryConnectionError(msg: String) extends DoorRepositoryError

  val ctx = new H2ZioJdbcContext(Literal)
  import ctx._

  val canConnect: ZIO[DataSource, DoorRepositoryError, Boolean] =
    for {
      _ <-
        ctx
          .run(
            query[DoorDAO]
          )
          .mapError(sqlException => DoorRepositoryConnectionError(sqlException.getMessage))
    } yield true

  def storeDoor(door: Door): ZIO[DataSource, DoorRepositoryError, Unit] =
    for {
      dao <- DoorDAO.doorToDaoZIO(door).mapError(throwable => DoorEncodingError(throwable.getMessage))
      success <-
        ctx
          .run(
            query[DoorDAO].insertValue(lift(dao))
          )
          .mapError(sqlException => DoorSavingError(sqlException.getMessage))
      _ <- IO.fail(DoorSavingError(s"Failed to save door: $door")).when(success == 0L)
    } yield ()

  def storeManyDoors(doors: Seq[Door]): ZIO[DataSource, DoorRepositoryError, Unit] =
    for {
      daos <- ZIO.foreach(doors)(DoorDAO.doorToDaoZIO).mapError(throwable => DoorEncodingError(throwable.getMessage))
      successes <-
        ctx
          .run(quote {
            liftQuery(daos).foreach(d => query[DoorDAO].insertValue(d))
          })
          .mapError(sqlException => DoorSavingError(sqlException.getMessage))
      failedDoors = successes.zipWithIndex.filter(_._1 == 0).map { case (_, index) => doors(index) }
      _ <- IO.fail(DoorSavingError(s"Failed to insert the doors: $failedDoors")).when(failedDoors.nonEmpty)
    } yield ()

  def retrieveDoorById(id: IntAbove0): ZIO[DataSource, DoorRepositoryError, Door] =
    for {
      daos <-
        ctx
          .run(
            quote {
              query[DoorDAO].filter(_.doorId == lift(id.value))
            }
          )
          .mapError(err => DoorRetrievalError(err.getMessage))
      doors <- daosToDoors(daos)
      door <- doors.headOption match {
        case Some(door) => ZIO.succeed(door)
        case None       => ZIO.fail(DoorNotFound(id))
      }
    } yield door

  def retrieveDoorsForTimezone(timezone: Timezone): ZIO[DataSource, DoorRetrievalError, Seq[Int]] =
    for {
      daos <-
        ctx
          .run(
            quote {
              query[DoorDAO].filter(_.timezone == lift(timezone.value)).map(_.doorId)
            }
          )
          .mapError(err => DoorRetrievalError(err.getMessage))
    } yield daos

  def deleteDoorById(id: IntAbove0): ZIO[DataSource, DoorRepositoryError, Unit] =
    for {
      success <-
        ctx
          .run(
            quote {
              query[DoorDAO].filter(_.doorId == lift(id.value)).delete
            }
          )
          .mapError(sqlException => DoorDeletionError(sqlException.getMessage))
      _ <- IO.fail(DoorDeletionError(s"Failed to delete door with ID: ${id.value}")).when(success == 0L)
    } yield ()

  private def daosToDoors(daos: Seq[DoorDAO]): IO[DoorDecodingError, Seq[Door]] =
    ZIO
      .foreach(daos) { dao =>
        ChildDAO.daoToChild(dao.child).flatMap { child =>
          Door.makeZIO(
            dao.doorId,
            (dao.latitude, dao.longitude),
            dao.timezone,
            child
          )
        }
      }
      .mapError(DoorDecodingError)
}
