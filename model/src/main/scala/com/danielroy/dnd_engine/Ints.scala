package com.danielroy.dnd_engine

import eu.timepit.refined.api.{Refined, RefinedTypeOps}
import eu.timepit.refined.numeric.Greater
import eu.timepit.refined.numeric.Interval.Closed
import zio.{IO, ZIO}

object Ints {
  type IntAbove0 = Int Refined Greater[0]
  object IntAbove0 extends RefinedTypeOps[IntAbove0, Int] {
    def fromIntZIO(t: Int): IO[String, IntAbove0] = ZIO.fromEither(super.from(t))
  }

  type Int10Value = Int Refined Closed[1, 10]
  object Int10Value extends RefinedTypeOps[Int10Value, Int] {
    def fromZIO(t: Int): IO[String, Int10Value] = {
      ZIO.fromEither(from(t))
    }
    final val MIN = 1
    final val MAX = 10
  }
  implicit class Int10ValueFunctions(my: Int10Value) {
    def <(that: Int10Value): Boolean = {
      my.value < that.value
    }
  }

  type Int20Value = Int Refined Closed[1, 20]
  object Int20Value extends RefinedTypeOps[Int20Value, Int] {
    def fromZIO(t: Int): IO[String, Int20Value] = {
      ZIO.fromEither(from(t))
    }
    final val MIN = 1
    final val MAX = 20
  }
  implicit class Int20ValueFunctions(my: Int20Value) {
    def <(that: Int20Value): Boolean = {
      my.value < that.value
    }
  }

  case class Int10To20Range private (low: Int10Value, high: Int20Value)
  object Int10To20Range {
    def make(low: Int, high: Int): IO[String, Int10To20Range] =
      ZIO.fromEither(for {
        lowInt  <- Int10Value.from(low)
        highInt <- Int20Value.from(high)
      } yield Int10To20Range(lowInt, highInt))
  }

  case class Int10Range private (low: Int10Value, high: Int10Value)
  object Int10Range {
    def make(low: Int10Value, high: Int10Value): Either[String, Int10Range] = {
      if (low < high) Right(Int10Range(low, high))
      else Left(s"The low value '$low' must be lower than the high value '$high'")
    }

    def makeZIO(low: Int10Value, high: Int10Value): IO[String, Int10Range] =
      ZIO.fromEither(make(low, high))
  }

}
