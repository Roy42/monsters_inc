package com.danielroy.monsters_inc.generator

import com.danielroy.monsters_inc.Monster
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineV
import zhttp.service.{ChannelFactory, EventLoopGroup}
import zio.ZIO
import zio.test.Assertion.{equalTo, fails}
import zio.test._

object MonsterGeneratorSpec extends DefaultRunnableSpec {

  private def assertWellFormedMonster(monster: Monster) = {
    assertTrue(monster.strengths.value.nonEmpty)
    assertTrue(monster.name.value.length > 4)
  }

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("MonsterGenerator test spec")(
      test("Create 2 monsters") {
        for {
          startingId <- ZIO.fromEither(refineV[Positive](1))
          monstersToSpawn <- ZIO.fromEither(refineV[Positive](2))
          monsters <- MonsterGenerator.generate(startingId, monstersToSpawn)
        } yield {
          assertTrue(monsters.length == 1)
          assertTrue(monsters.head.id.value == 1)
          assertWellFormedMonster(monsters.head)
        }
      },

      test("Create 1000 randomized monsters") {
        for {
          startingId <- ZIO.fromEither(refineV[Positive](1))
          monstersToSpawn <- ZIO.fromEither(refineV[Positive](1000))
          monsters <- MonsterGenerator.generate(startingId, monstersToSpawn)
        } yield assertTrue(
          monsters.length == 1000
        )
      }
    ).provide(TestRandom.make(TestRandom.Data(7, 23)))
}
