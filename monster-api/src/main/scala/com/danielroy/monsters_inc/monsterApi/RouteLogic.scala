package com.danielroy.monsters_inc.monsterApi

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.database.MonsterRepository
import zhttp.http._
import zio._
import zio.json.EncoderOps

import javax.sql.DataSource

object RouteLogic {

  def getAllMonsters: ZIO[DataSource, Throwable, Response] =
    for {
      ids <- MonsterRepository.retrieveAllMonsterIds().orElseFail(new IllegalStateException("Failed to find any monsters in storage"))
    } yield Response.json(ids.toJson)

  def getMonster(monsterId: => String): ZIO[DataSource, Throwable, Response] =
    for {
      id <- IntAbove0.fromIntZIO(monsterId.toInt).mapError(str => new IllegalStateException(str))
      monster <- MonsterRepository.retrieveMonsterById(id).orElseFail(new IllegalStateException(s"Did not find Monster with ID: $monsterId"))
    } yield Response.json(monster.toJson)

  def canConnectToRepository: ZIO[DataSource, Throwable, Response] =
    for {
      b <- MonsterRepository.canConnect.orElseFail(new IllegalStateException(s"Failed to connect to MonsterRepository"))
    } yield if (b) Response.ok else Response.text("no")
}
