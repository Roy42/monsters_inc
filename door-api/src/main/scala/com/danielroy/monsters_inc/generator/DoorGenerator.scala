package com.danielroy.monsters_inc.generator

import com.danielroy.dnd_engine.Ints.{Int10Value, Int20Value}
import com.danielroy.monsters_inc.Fear.fromInt
import com.danielroy.monsters_inc.{Child, Door, Timezone}
import com.danielroy.randomData.RandomPerson._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Positive
import zio.Random.nextIntBetween
import zio.{Random, ZIO}

object DoorGenerator {

  def generate(startingId: Int Refined Positive, num: Int Refined Positive): ZIO[Random, String, Seq[Door]] =
    for {
      ids <- ZIO.succeed(startingId.value to num.value + 1)
      randomPeople <- generateRandomPeople(num.value).mapError(t => t.getMessage)
      doors <- ZIO.foreach(randomPeople.zipWithIndex) { case (person, i) =>
        for {
          heavySleeper <- nextIntBetween(Int10Value.MIN, Int10Value.MAX)
          screamChance <- nextIntBetween(Int20Value.MIN, Int20Value.MAX)
          intensityLow <- nextIntBetween(Int10Value.MIN, Int10Value.MAX - 1)
          intensityHigh <- nextIntBetween(intensityLow + 1, Int10Value.MAX)
          numOfFears <- nextIntBetween(1, 8)
          timezone <- nextIntBetween(Timezone.MIN, Timezone.MAX + 1)
          fears = (1 to numOfFears).map(fromInt).toSet
          id <- ZIO.succeed(ids(i))
          child <- Child.makeZIO(id, person.name, fears, heavySleeper, screamChance, (intensityLow, intensityHigh))
          door <- Door.makeZIO(id, person.location, timezone, child)
        } yield door
      }
    } yield doors

}
