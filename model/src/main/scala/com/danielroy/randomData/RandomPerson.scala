package com.danielroy.randomData

import zio.json._
import zio.{Random, Task, URIO, ZIO}

import scala.collection.mutable.ListBuffer
import scala.io.BufferedSource

object RandomPerson {

  // https://data.world/alexandra/baby-names
  private final val FIRST_NAME_FILE = ("first_names.txt", 6782)
  // https://data.world/uscensusbureau/frequently-occurring-surnames-from-the-census-2010
  private final val LAST_NAME_FILE = ("last_names.txt", 1000)

  /**
    * Monsters Inc spec for a random person
    */
  case class RandomPerson(name: String, location: (Double, Double))
  implicit val randomPersonEncoder: JsonEncoder[RandomPerson] = DeriveJsonEncoder.gen
  implicit val randomPersonDecoder: JsonDecoder[RandomPerson] = DeriveJsonDecoder.gen

  private def getRandomNames(file: String, linesInFile: Int, numberOfNames: Int): Task[Seq[String]] = {
    val acquireFile: Task[BufferedSource] = ZIO.attempt(scala.io.Source.fromResource(file))

    // TODO: handle file release failure properly
    val releaseFile: BufferedSource => URIO[Any, Unit] = source => ZIO.attempt(source.close()).orDie

    val useFile: BufferedSource => Task[Seq[String]] = source =>
      {
        ZIO
          .collectAll {
            // The indexes of the names in the file we will take
            for (_ <- 1 to numberOfNames)
              yield Random.nextIntBetween(0, linesInFile)
          }
          .map { indexes =>
            // Sort and then convert the indexes of the names in the list we will take to "jumps"
            // Where each jump is a number >= 0 of how many times we need to next() the iterator to get to the next index
            // This way, we only need to run through the file iterator once
            // Start with -1 so the first jump is guaranteed to be >0
            val jumps = (-1 +: indexes).sorted.iterator.sliding(2)
            val names = source.getLines()

            // TODO: prefer not to use a mutable data structure here. Maybe a tailrec function?
            val namesToUse = ListBuffer.empty[String]
            while (jumps.hasNext) {
              val Seq(left, right) = jumps.next()
              val jump             = right - left
              val name             = if (jump == 0) namesToUse.last else (for (_ <- 1 to jump) yield names.next()).last
              namesToUse += name
            }
            namesToUse.toList
          }
      }.provideLayer(Random.live) // TODO: shouldn't be providing the layer here

    ZIO.acquireReleaseWith(acquireFile, releaseFile, useFile)
  }

  private val randomLatitude: ZIO[Random, Nothing, Double] =
    for {
      num <- Random.nextDoubleBetween(-45.0, 45.0)
    } yield (math floor num * 100000) / 100000

  private val randomLongitude: ZIO[Random, Nothing, Double] =
    for {
      num <- Random.nextDoubleBetween(-90.0, 90.0)
    } yield (math floor num * 100000) / 100000

  def generateRandomPeople(num: Int): ZIO[Random, Throwable, Seq[RandomPerson]] =
    for {
      forenames <- getRandomNames(FIRST_NAME_FILE._1, FIRST_NAME_FILE._2, num)
      surnames  <- getRandomNames(LAST_NAME_FILE._1, LAST_NAME_FILE._2, num)
      names     <- ZIO.attempt(forenames.zip(surnames).map { case (forename, surname) => s"$forename $surname" })
      people <- ZIO.foreach(names) { name =>
        for {
          lat <- randomLatitude
          lon <- randomLongitude
        } yield RandomPerson(name, (lat, lon))
      }
    } yield people

  /**
  // OLD IMPLEMENTATION, USES EXTERNAL API

  private final val URL = "https://randomuser.me/api/"

  /**
   * RandomUser API spec
   */
  private case class RandomUserApiCoordinates(latitude: Double, longitude: Double)
  private case class RandomUserApiLocation(coordinates: RandomUserApiCoordinates)
  private case class RandomUserApiName(first: String, last: String)
  private case class RandomUserApiPerson(name: RandomUserApiName, location: RandomUserApiLocation)
  private case class RandomUserApiResult(results: List[RandomUserApiPerson])
  private implicit val randomUserApiCoordinatesDecoder: JsonDecoder[RandomUserApiCoordinates] = DeriveJsonDecoder.gen
  private implicit val randomUserApiLocationDecoder: JsonDecoder[RandomUserApiLocation]       = DeriveJsonDecoder.gen
  private implicit val randomUserApiNameDecoder: JsonDecoder[RandomUserApiName]               = DeriveJsonDecoder.gen
  private implicit val randomUserApiPersonDecoder: JsonDecoder[RandomUserApiPerson]           = DeriveJsonDecoder.gen
  private implicit val randomUserApiResultDecoder: JsonDecoder[RandomUserApiResult]           = DeriveJsonDecoder.gen

  private def randomUserApiResultToRandomPerson(res: RandomUserApiResult): IO[String, RandomPerson] =
    ZIO
      .attempt {
        val result = res.results.head
        val name   = s"${result.name.first} ${result.name.last}"
        val lat    = result.location.coordinates.latitude
        val lon    = result.location.coordinates.longitude
        RandomPerson(name, (lat, lon))
      }
      .mapError(_.getMessage)

  val genRandomPerson: ZIO[EventLoopGroup with ChannelFactory, String, RandomPerson] =
    for {
      res                 <- Client.request(URL).mapError(_.getMessage)
      data                <- res.bodyAsString.mapError(_.getMessage)
      randomUserApiResult <- ZIO.fromEither(data.fromJson[RandomUserApiResult])
      randomPerson        <- randomUserApiResultToRandomPerson(randomUserApiResult)
    } yield randomPerson
*/
}
