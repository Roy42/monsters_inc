package com.danielroy.monsters_inc

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.dnd_engine.JsonCoders._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.GreaterEqual
import eu.timepit.refined.refineV
import zio.{IO, ZIO}
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class ScareReport(doorId: IntAbove0, monsterId: IntAbove0, timeTaken: IntAbove0, scareVolume: Int Refined GreaterEqual[0])

object ScareReport {

  def makeZIO(doorId: Int, monsterId: Int, timeTaken: Int, scareVolume: Int): IO[String, ScareReport] =
    for {
      doorId <- IntAbove0.fromIntZIO(doorId)
      monsterId <- IntAbove0.fromIntZIO(monsterId)
      timeTaken <- IntAbove0.fromIntZIO(timeTaken)
      scareVolume <- ZIO.fromEither(refineV[GreaterEqual[0]](scareVolume))
    } yield ScareReport(doorId, monsterId, timeTaken, scareVolume)

  implicit val scareReportJsonEncoder: JsonEncoder[ScareReport] = DeriveJsonEncoder.gen
  implicit val scareReportJsonDecoder: JsonDecoder[ScareReport] = DeriveJsonDecoder.gen
}