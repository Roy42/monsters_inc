package com.danielroy.monsters_inc

package object doorApi {
  trait DoorApiError
  case class DoorApiStartupError(msg: String) extends DoorApiError
}
