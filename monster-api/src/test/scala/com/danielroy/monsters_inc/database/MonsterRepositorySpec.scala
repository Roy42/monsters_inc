package com.danielroy.monsters_inc.database

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.database.MonsterRepository.MonsterNotFound
import com.danielroy.monsters_inc.{Fear, FearStrength, Monster}
import io.getquill.context.ZioJdbc.DataSourceLayer
import zio.ZLayer
import zio.test.Assertion.{contains, equalTo, fails}
import zio.test._

import javax.sql.DataSource

object MonsterRepositorySpec extends DefaultRunnableSpec {

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val fsZIO =
    for {
      fs1 <- Monster.makeFearStrengthZIO(Fear.Darkness, 5)
      fs2 <- Monster.makeFearStrengthZIO(Fear.Clowns, 19)
    } yield Seq(fs1, fs2)

  private def testMonster(id: Int) = fsZIO.flatMap { fs =>
    Monster.makeZIO(id, "Mike Sullivan", fs, 12, 15)
  }

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("MonsterRepository test spec")(
      test("Save and then get a monster") {
        for {
          monster <- testMonster(1)
          _ <- MonsterRepository.storeMonster(monster)
          outMonster <- MonsterRepository.retrieveMonsterById(monster.id)
          _ <- MonsterRepository.deleteMonsterById(monster.id)
        } yield assertTrue(outMonster == monster)
      },
      test("Save and then get several monsters") {
        for {
          monster2 <- testMonster(2)
          monster3 <- testMonster(3)
          _ <- MonsterRepository.storeMonster(monster2)
          _ <- MonsterRepository.storeMonster(monster3)
          ids <- MonsterRepository.retrieveAllMonsterIds()
        } yield {
          assertTrue(ids.containsSlice(Seq(2, 3)))
        }
      },
      test("Fail when retrieving monster by ID for non-existent ID") {
        for {
          id <- IntAbove0.fromIntZIO(9999)
          idLong <- MonsterRepository.retrieveMonsterById(id).exit
        } yield assert(idLong)(fails(equalTo(MonsterNotFound(id))))
      }
    ).provideCustomLayer(DataSource)
}
