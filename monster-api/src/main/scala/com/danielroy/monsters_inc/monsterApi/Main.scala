package com.danielroy.monsters_inc.monsterApi

import com.danielroy.monsters_inc.database.MonsterRepository
import com.danielroy.monsters_inc.generator.MonsterGenerator
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineV
import io.getquill.context.ZioJdbc.DataSourceLayer
import zhttp.service.{EventLoopGroup, Server, ServerChannelFactory}
import zhttp.service.server.ServerChannelFactory
import zio._
import zio.config.ReadError
import zio.config.typesafe.TypesafeConfig

import javax.sql.DataSource

object Main extends App {

  private val makeConfigLayer =
    System
      .env("MONSTER_API_CONF")
      .someOrFail("Please specify the Monster API config file in the system environment variable MONSTER_API_CONF")
      .foldZIO(
        _ => ZIO.fail(new IllegalArgumentException("Did not find the config file")),
        { configFilePath =>
          val file = new java.io.File(configFilePath)
          if (file.isFile) {
            ZIO.attempt(TypesafeConfig.fromHoconFile(file, MonsterApiConfig.configDescriptor))
          } else {
            ZIO.fail {
              val filesInDir = file.getParentFile.listFiles().map(_.getName).mkString(",")
              new IllegalArgumentException(s"Did not find the config file: $configFilePath. Did you mean any of these? $filesInDir")
            }
          }
        }
      )

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val makeServer: ZIO[MonsterApiConfig, Nothing, Server[DataSource, Throwable]] =
    for {
      appConfig <- ZIO.service[MonsterApiConfig]
      server =
        Server.port(appConfig.port.value) ++
          Server.paranoidLeakDetection ++
          Server.app(Routes.routes)
    } yield server

  private val makeServerChannelFactory: URIO[MonsterApiConfig, ZLayer[Any, Nothing, ServerChannelFactory with EventLoopGroup]] =
    for {
      config <- ZIO.service[MonsterApiConfig]
    } yield ServerChannelFactory.auto ++ EventLoopGroup.auto(config.threads.value)

  private val generateRandomMonsters =
    for {
      appConfig <- ZIO.service[MonsterApiConfig]
      startingId <- ZIO.fromEither(refineV[Positive](1))
      monstersToSpawn <- ZIO.fromEither(refineV[Positive](appConfig.monstersToSpawn.value))
      monsters <- MonsterGenerator.generate(startingId, monstersToSpawn)
      res <- ZIO.foreachPar(monsters)(MonsterRepository.storeMonster)
    } yield ()

  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    // First unsafe run to collect the configuration
    val configLayer: ZLayer[Any, ReadError[String], MonsterApiConfig] = unsafeRun(makeConfigLayer)

    val server = for {
      appConfig   <- ZIO.service[MonsterApiConfig]
      serverChannelLayer <- makeServerChannelFactory
      server      <- makeServer
      _ <- generateRandomMonsters.provideLayer(
        DataSource ++
          Random.live ++
          configLayer
      )
    } yield server.make
      .use(_ =>
        Console.printLine(s"Monster API started at ${appConfig.host}:${appConfig.port}")
          *> Console.printLine(s"With configuration: $appConfig")
          *> ZIO.never
      )
      .provideCustomLayer(serverChannelLayer ++ DataSource)
      .exitCode

    // Second and final unsafe run to boot up the API server
    unsafeRun(
      server.provideLayer(
        System.live ++ configLayer
      )
    )
  }
}
