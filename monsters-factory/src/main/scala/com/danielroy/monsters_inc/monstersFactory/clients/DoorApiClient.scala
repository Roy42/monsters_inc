package com.danielroy.monsters_inc.monstersFactory.clients

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.monstersFactory.MonstersFactoryConfig
import com.danielroy.monsters_inc.{Door, Timezone}
import zhttp.http.Status
import zhttp.service.Client
import zio.ZIO
import zio.json.DecoderOps

object DoorApiClient {

  def getDoor(doorId: IntAbove0): ZIO[ClientDeps, ClientError, Door] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.doorApiUrl.value + "/door/" + doorId.value
      response <- Client.request(url).mapError(t => DoorApiClientError(t.getMessage))
      body <- response.bodyAsString.mapError(t => ParsingError(t.getMessage))
      door <- ZIO.fromEither(body.fromJson[Door]).mapError(s => ParsingError(s))
    } yield door

  def getDoorSet(timezone: Timezone): ZIO[ClientDeps, ClientError, Seq[Int]] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.doorApiUrl.value + "/doorSet/" + timezone.value
      response <- Client.request(url).mapError(t => DoorApiClientError(t.getMessage))
      body <- response.bodyAsString.mapError(t => ParsingError(t.getMessage))
      ids <- ZIO.fromEither(body.fromJson[Seq[Int]]).mapError(s => ParsingError(s))
    } yield ids

  val apiIsReady: ZIO[ClientDeps, ClientError, Boolean] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.doorApiUrl.value + "/ready"
      response <- Client.request(url).mapError(t => DoorApiClientError(t.getMessage))
    } yield response.status == Status.OK
}
