package com.danielroy.monsters_inc.monstersFactory

import com.danielroy.dnd_engine.Ints.IntAbove0
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import zio.Duration

case class MonstersFactoryConfig(
    doorApiUrl: String Refined Url,
    monsterApiUrl: String Refined Url,
    clientThreads: IntAbove0,
    serveTimeTaken: Boolean,
    printScareReports: Boolean,
    startupDelay: Duration,
    numTimesToWaitForStartup: IntAbove0
)

object MonstersFactoryConfig {

  import zio.config._
  import zio.config.refined._ // IntelliJ marks this as unused but it actually is
  import zio.config.magnolia.DeriveConfigDescriptor.descriptor

  val configDescriptor: ConfigDescriptor[MonstersFactoryConfig] =
    descriptor[MonstersFactoryConfig]
      .mapKey(toKebabCase)
      .mapKey(_.toLowerCase)
}
