package com.danielroy.monsters_inc.doorApi

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.Door._
import com.danielroy.monsters_inc._
import com.danielroy.monsters_inc.database.DoorRepository
import zhttp.http.Response
import zio._
import zio.json.EncoderOps
import javax.sql.DataSource

object RouteLogic {
  // TODO: I don't like having to use Throwable here, would prefer to use our ServerError trait
  // But it appears we are forced to by zio-http

  // TODO: handle the DoorRepository errors properly, convert to Throwable?

  def getDoorsForTimezone(timezoneInt: => Int): ZIO[DataSource, Throwable, Response] =
    for {
      timezone <- Timezone.fromZIO(timezoneInt).orElseFail(new IllegalArgumentException(s"Failed to convert $timezoneInt to a Timezone"))
      doorIds <- DoorRepository.retrieveDoorsForTimezone(timezone).orElseFail(new IllegalStateException(s"Did not find doors for timezone: $timezoneInt"))
    } yield Response.json(doorIds.toJson)

  def getDoor(doorId: => String): ZIO[DataSource, Throwable, Response] =
    for {
      id <- IntAbove0.fromIntZIO(doorId.toInt).mapError(str => new IllegalStateException(str))
      door <- DoorRepository.retrieveDoorById(id).orElseFail(new IllegalStateException(s"Did not find Door with ID: $doorId"))
    } yield Response.json(door.toJson)

  def canConnectToRepository: ZIO[DataSource, Throwable, Response] =
    for {
      b <- DoorRepository.canConnect.orElseFail(new IllegalStateException(s"Failed to connect to DoorRepository"))
    } yield if (b) Response.ok else Response.text("no")
}
