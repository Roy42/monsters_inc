package com.danielroy.monsters_inc.database

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.{FearStrength, Monster}
import com.danielroy.monsters_inc.database.daos.MonsterDAO
import io.getquill._
import zio.json.DecoderOps
import zio.{IO, ZIO}

import javax.sql.DataSource

object MonsterRepository {

  sealed trait MonsterRepositoryError
  case class MonsterSavingError(msg: String)               extends MonsterRepositoryError
  case class MonsterEncodingError(msg: String)             extends MonsterRepositoryError
  case class MonsterRetrievalError(msg: String)            extends MonsterRepositoryError
  case class MonsterDecodingError(msg: String)             extends MonsterRepositoryError
  case class MonsterNotFound(idNotFound: IntAbove0)        extends MonsterRepositoryError
  case class MonsterDeletionError(msg: String)             extends MonsterRepositoryError
  case class MonsterRepositoryConnectionError(msg: String) extends MonsterRepositoryError

  val ctx = new H2ZioJdbcContext(Literal)
  import ctx._

  val canConnect: ZIO[DataSource, MonsterRepositoryError, Boolean] =
    for {
      _ <-
        ctx
          .run(
            query[MonsterDAO]
          )
          .mapError(sqlException => MonsterRepositoryConnectionError(sqlException.getMessage))
    } yield true

  def storeMonster(monster: Monster): ZIO[DataSource, MonsterRepositoryError, Unit] =
    for {
      dao <- MonsterDAO.monsterToDaoZIO(monster).mapError(throwable => MonsterEncodingError(throwable.getMessage))
      success <-
        ctx
          .run(
            quote {
              query[MonsterDAO].insertValue(lift(dao))
            }
          )
          .mapError(sqlException => MonsterSavingError(sqlException.getMessage))
      _ <- IO.fail(MonsterSavingError(s"Failed to save monster: $monster")).when(success == 0L)
    } yield ()

  def retrieveMonsterById(id: IntAbove0): ZIO[DataSource, MonsterRepositoryError, Monster] =
    for {
      monsterDaos <-
        ctx
          .run(
            quote {
              query[MonsterDAO].filter(_.monsterId == lift(id.value))
            }
          )
          .mapError(err => MonsterRetrievalError(err.getMessage))
      monsters <- daosToMonsters(monsterDaos)
      monster <- monsters.headOption match {
        case Some(monster) => ZIO.succeed(monster)
        case None          => ZIO.fail(MonsterNotFound(id))
      }
    } yield monster

  def retrieveAllMonsterIds(): ZIO[DataSource, MonsterRepositoryError, Seq[Int]] =
    for {
      ids <-
        ctx
          .run(
            quote {
              query[MonsterDAO].map(_.monsterId)
            }
          )
          .mapError(err => MonsterRetrievalError(err.getMessage))
    } yield ids

  def deleteMonsterById(id: IntAbove0): ZIO[DataSource, MonsterRepositoryError, Unit] =
    for {
      success <-
        ctx
          .run(
            quote {
              query[MonsterDAO].filter(_.monsterId == lift(id.value)).delete
            }
          )
          .mapError(sqlException => MonsterDeletionError(sqlException.getMessage))
      _ <- IO.fail(MonsterDeletionError(s"Failed to delete door with ID: ${id.value}")).when(success == 0L)
    } yield ()

  private def daosToMonsters(daos: Seq[MonsterDAO]): ZIO[Any, MonsterRepositoryError, Seq[Monster]] = {
    ZIO
      .foreach(daos) { dao =>
        ZIO.fromEither(dao.strengths.fromJson[Seq[FearStrength]]).flatMap { fs =>
          Monster.makeZIO(
            dao.monsterId,
            dao.name,
            fs,
            dao.endurance,
            dao.effectiveness
          )
        }
      }
      .mapError(MonsterDecodingError)
  }
}
