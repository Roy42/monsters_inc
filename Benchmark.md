# Monsters Factory Benchmark Results

All benchmarks performed with the time taken penalty served. #

## Increasing Door Count
| Doors | Monsters | Time Taken (s) |
|-------|----------|----------------|
| 501   | 50       | 418            |
| 1000  | 50       | 469            |
| 2000  | 50       | 521            |
| 4000  | 50       | 774            |

## Increasing Monster Count
| Doors | Monsters | Time Taken (s) |
|-------|----------|----------------|
| 2000  | 50       | 521            |
| 2000  | 100      | 508            |
| 2000  | 150      | 503            |
| 2000  | 200      | 484            |