package com.danielroy.monsters_inc

import com.danielroy.monsters_inc.Fear._
import zio._

package object monsters {

  def getRandomFear: URIO[Random, Fear] = {
    for {
      randomInt <- Random.nextIntBetween(0, 10)
    } yield {
      randomInt match {
        case 0 => Darkness
        case 1 => Shadows
        case 2 => Spiders
        case 3 => Snakes
        case 4 => Clowns
        case 5 => Dogs
        case 6 => Storms
        case 7 => Needles
        case 8 => Germs
        case 9 => Blood
      }
    }
  }

}
