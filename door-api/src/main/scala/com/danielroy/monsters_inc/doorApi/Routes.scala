package com.danielroy.monsters_inc.doorApi

import zio.{ZIO, _}
import zhttp.http._
import javax.sql.DataSource

object Routes {

  private def firstPathValue(path: Path): Task[String] = ZIO.attempt {
    path.last.getOrElse(throw new IllegalArgumentException("Empty path parameter"))
  }

  final val routes: HttpApp[DataSource, Throwable] = Http.collectZIO[Request] {
    case Method.GET -> "doorSet" /: timezonePath =>
      for {
        timezone <- firstPathValue(timezonePath).map(_.toInt)
        response  <- RouteLogic.getDoorsForTimezone(timezone)
      } yield response

    case Method.GET -> "door" /: doorIdPath =>
      for {
        doorId   <- firstPathValue(doorIdPath)
        response <- RouteLogic.getDoor(doorId)
      } yield response

    case Method.GET -> !! / "alive" =>
      UIO(Response.ok)

    case Method.GET -> !! / "ready" =>
      RouteLogic.canConnectToRepository
  }

}
