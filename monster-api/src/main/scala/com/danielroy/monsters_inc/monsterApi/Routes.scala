package com.danielroy.monsters_inc.monsterApi

import zhttp.http._
import zio._

import javax.sql.DataSource

object Routes {

  private def firstPathValue(path: Path): Task[String] = ZIO.attempt {
    path.last.getOrElse(throw new IllegalArgumentException("Empty path parameter"))
  }

  final val routes: HttpApp[DataSource, Throwable] = Http.collectZIO[Request] {
    case Method.GET -> !! / "all" =>
      for {
        response  <- RouteLogic.getAllMonsters
      } yield response

    case Method.GET -> "monster" /: doorIdPath =>
      for {
        doorId   <- firstPathValue(doorIdPath)
        response <- RouteLogic.getMonster(doorId)
      } yield response

    case Method.GET -> !! / "alive" =>
      UIO(Response.ok)

    case Method.GET -> !! / "ready" =>
      RouteLogic.canConnectToRepository
  }
}
