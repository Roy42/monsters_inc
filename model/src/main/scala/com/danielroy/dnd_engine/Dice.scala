package com.danielroy.dnd_engine

import zio.{Random, URIO, ZIO}

object Dice {

  val rollD20: URIO[Random, Int] =
    Random.nextIntBetween(1, 21)

  val rollD10: URIO[Random, Int] =
    Random.nextIntBetween(1, 11)
}
