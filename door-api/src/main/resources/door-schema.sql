CREATE TABLE IF NOT EXISTS DoorDAO(
    doorId int NOT NULL,
    latitude float,
    longitude float,
    timezone int,
    childId int,
    name varchar(50),
    fears varchar(25),
    heavySleeper int,
    screamChance int,
    screamIntensityLow int,
    screamIntensityHigh int,
    lastScared varchar(50),
    PRIMARY KEY (doorId)
)