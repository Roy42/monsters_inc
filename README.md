# ZIO Demo Project: Monsters Inc - Daniel Roy

**Disclaimer: This is an entirely non-profit project, created for fun and personal study**

This is a personal project created solely by me to demonstrate my capability and understanding of the ZIO framework.

This is my first attempt at creating anything for ZIO. As I learn more, I will improve and add more to the project.

This project is not intended to be perfect. There's a lack of testing for one. Instead, view it as a playground
of experimentation of ideas and concepts.

If you have any advice, please message me! I am new to the ZIO ecosystem and am eager to learn more.

## The Monsters Inc. Theme

Thinking of interesting ideas for demo projects is always hard, particularly finding something that is entertaining not
only for me but also you, the reader, potentially a recruiter, interviewer or someone trying to learn a bit of ZIO
too. 

Inspiration struck me when watching the terrific Pixar movie Monsters Inc. My idea is that the power generation factory in the
movie where Mike and Sulley work could serve as a basis for my project, whereby I simulate the daily activity of the
monsters as they generate electricity through the scaring of children, brought to them via magic portal doors.

The structure of the project is as follows:
* A Door API, generates Doors with random values and serves them on demand
* A Monster API, generates Monsters with random values and serves them on demand
* A Monster Factory, runs a full day simulation of the Monster Factory. Monsters are matched with Doors, and a scare
    report is generated based on the stats of each, like a video game. Composes a summary report at the end.

The project will is runnable via a Docker Compose file, where the output is seen via the logs of the Monsters 
Factory container.

All modules will be based on Scala ZIO version 2.0.0-RC2, as I wanted to use ZIO version 2 and this particular RC version
worked best with the other libraries I used.

## Requirements to run:
* Docker
* Docker Compose
* make
* sbt

## How to Run:

1. Ensure you have all the requirements installed listed above
2. Checkout the Git repository
3. Run `make buildDocker` to build the Docker images and publish to your local repository
4. Run `make startFactory` to start the simulation
   1. You will see the logs of the Monsters Factory in the terminal, publish Scare Reports as they come in
   2. Once complete, a summary report will be printed
5. Run `make stopFactory` to shut down all the services

## Skills Demonstrated

Below are the skills demonstrated in this project and some (not all) locations where you can find examples.

| Skill          | Example                                                                          |
|----------------|----------------------------------------------------------------------------------|
| Scala          | Self-Evident                                                                     |
| ZIO            | Self-Evident, monsters-factory for exclusively ZIO code instead of zio-http      |
| ZIO HTTP       | Door-API & Monster-API - Main.scala, Routes.scala                                |
| ZIO JSON       | Model - package.scala, Door.scala, Monster.scala                                 |
| ZIO Test       | Door-API - DoorGeneratorSpec.scala                                               |
| ZIO Config     | Door-API & Monster-API - Main.scala, DoorApiConfig.scala, MonsterApiConfig.scala |
| ZIO Quill      | Door-API & Monster-API - DoorRepository.scala, MonsterRepository.scala           |
| Docker         | Makefile, build.sbt                                                              |
| Docker Compose | docker-compose.yml                                                               |


## TODO - Essentials
* ~~Create Project structure~~
* ~~Define core model~~
* ~~Define Door API spec~~
* ~~Door generator using randomised values~~
* ~~Define Monster API spec~~
* ~~Monster generator using randomised values~~
* ~~Add configuration to APIs~~
* ~~Database backend for storing Monsters and Doors~~
* ~~Generate Monsters and Doors on startup~~
* ~~Monsters Factory~~
  * ~~Prepare lifecycle (loop on timezones)~~
  * ~~Per loop~~
    * ~~Get doors~~
    * ~~Get monsters for shift~~
    * ~~Run each monster against it's next door (using the DnD engine)~~
    * ~~Store result~~
  * ~~Display results to Console~~
* ~~Create Docker images~~
  * ~~Door API~~
  * ~~Monster API~~
  * ~~Monsters Factory~~
* ~~Create Docker Compose~~

## TODO - Bonus
* Update OpenAPI spec files
* Introduce common dataset stored in the database for DoorApi and MonsterApi tests
* Better error handling in the DoorApi and MonsterApi
  * I believe errors are being swallowed up and not surfacing to the end user in any form
  * Proper project wide error type hierarchy
* ZIO Logging
* ZIO ZMX (Metrics)
* ZIO Telemetry