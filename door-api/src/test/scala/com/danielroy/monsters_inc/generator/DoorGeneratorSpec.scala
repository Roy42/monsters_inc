package com.danielroy.monsters_inc.generator

import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineV
import zhttp.service.{ChannelFactory, EventLoopGroup}
import zio.ZIO
import zio.test.Assertion.{equalTo, fails}
import zio.test.{DefaultRunnableSpec, TestEnvironment, TestRandom, ZSpec, assert, assertTrue}

object DoorGeneratorSpec extends DefaultRunnableSpec {

  override def spec: ZSpec[TestEnvironment, Any] =
    suite("Door Generator Spec")(
      test("Generate 2 doors, values are randomly generated") {
        for {
          startingId <- ZIO.fromEither(refineV[Positive](1))
          doorsToSpawn <- ZIO.fromEither(refineV[Positive](2))
          doors <- DoorGenerator.generate(startingId, doorsToSpawn)
        } yield {
          val door1 = doors.head
          val door2 = doors(1)
          assertTrue(
            door1.id.value != door2.id.value,
            door1.child.name.value != door2.child.name.value,
            door1.geoLocation != door2.geoLocation
          )
        }
      },

      test("Create 1000 randomized doors") {
        for {
          startingId <- ZIO.fromEither(refineV[Positive](1))
          doorsToSpawn <- ZIO.fromEither(refineV[Positive](1000))
          doors <- DoorGenerator.generate(startingId, doorsToSpawn)
        } yield assertTrue(
          doors.length == 1000
        )
      }
    ).provide(TestRandom.make(TestRandom.Data(13, 17)))
}
