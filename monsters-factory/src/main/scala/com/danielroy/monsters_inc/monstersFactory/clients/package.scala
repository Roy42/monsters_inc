package com.danielroy.monsters_inc.monstersFactory

import zhttp.service.{ChannelFactory, EventLoopGroup}

package object clients {

  type ClientDeps = EventLoopGroup with ChannelFactory with MonstersFactoryConfig

  sealed trait ClientError {
    val msg: String
  }
  case class DoorApiClientError(msg: String) extends ClientError
  case class MonsterApiClientError(msg: String) extends ClientError
  case class ParsingError(msg: String) extends ClientError

}
