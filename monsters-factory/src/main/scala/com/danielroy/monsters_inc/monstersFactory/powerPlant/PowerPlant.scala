package com.danielroy.monsters_inc.monstersFactory.powerPlant

import com.danielroy.dnd_engine.Dice
import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.database.ScareReportRepository
import com.danielroy.monsters_inc.monstersFactory.{MonstersFactoryConfig, clients}
import com.danielroy.monsters_inc.monstersFactory.clients.{ClientDeps, DoorApiClient, MonsterApiClient}
import com.danielroy.monsters_inc.{Door, FearStrength, Monster, ScareReport, Timezone}
import eu.timepit.refined.numeric.GreaterEqual
import eu.timepit.refined.refineV
import zio._
import zio.internal.RingBuffer
import zio.json.EncoderOps

import java.util.concurrent.TimeUnit
import javax.sql.DataSource

object PowerPlant {

  private final val STANDARD_TIME_TAKEN = 3

  sealed trait PowerPlantError {
    val msg: String
  }
  case class PowerPlantEncodingError(msg: String)     extends PowerPlantError
  case class PowerPlantDoorApiError(msg: String)      extends PowerPlantError
  case class PowerPlantDoorQueueError(msg: String)    extends PowerPlantError
  case class PowerPlantMonsterApiError(msg: String)   extends PowerPlantError
  case class PowerPlantFilingError(msg: String)       extends PowerPlantError
  case class PowerPlantAnnouncementError(msg: String) extends PowerPlantError
  case class PowerPlantConsoleError(msg: String)      extends PowerPlantError
  case class PowerPlantStartupError(msg: String)      extends PowerPlantError

  /**
    * The monster works, pulling from the door queue one at a time and attempts to scare the child.
    * Generates a resultant scare volume and a report.
    */
  private def work(monster: Monster, doorBuffer: RingBuffer[Int]): ZIO[Console with Random with Clock with ClientDeps, PowerPlantError, Seq[ScareReport]] = {
    val nextDoorId = doorBuffer.poll(-1)
    if (nextDoorId < 0) {
      ZIO.succeed(Seq.empty[ScareReport])
    } else {
      val report = for {
        doorId <- IntAbove0.fromIntZIO(nextDoorId).mapError(PowerPlantEncodingError)
        door   <- DoorApiClient.getDoor(doorId).mapError(err => PowerPlantDoorApiError(s"Error while retrieving Doors: $err"))
        report <- sendMonsterThroughDoor(monster, door)
      } yield report
      // Loop and zip the results
      report.zipWith(work(monster, doorBuffer))((a, b) => b :+ a)
    }
  }

  /**
    * The monster enters a door and attempts to scare the child.
    * Time taken is determined by the monster's effectiveness.
    * Returns the volume of scream generated. Can be 0.
    */
  private def sendMonsterThroughDoor(monster: Monster, door: Door): ZIO[Console with Random with Clock with MonstersFactoryConfig, PowerPlantEncodingError, ScareReport] = {
    val childFears           = door.child.fears.value.toSeq
    val childScreamChance    = door.child.screamChance.value
    val childScreamIntensity = door.child.screamIntensity

    val serveTimeTaken: ZIO[Clock with Random with MonstersFactoryConfig, Nothing, Int] = for {
      appConfig <- ZIO.service[MonstersFactoryConfig]
      timeTaken <- Dice.rollD20
      totalTimeTaken = STANDARD_TIME_TAKEN + scala.math.max(timeTaken - monster.effectiveness.value, 0)
      _ <- ZIO.sleep(totalTimeTaken.seconds).when(appConfig.serveTimeTaken)
    } yield totalTimeTaken

    val results: ZIO[Random, Nothing, Iterable[Int]] =
      ZIO.foreach(monster.strengths.value) {
        case FearStrength(fear, potency) =>
          if (childFears.contains(fear)) {
            for {
              d20 <- Dice.rollD20
              result <- {
                val diff = d20 - potency.value
                if (diff >= 0) Dice.rollD10
                else ZIO.succeed(0)
              }
            } yield result
          } else ZIO.succeed(0)
      }

    (for {
      appConfig        <- ZIO.service[MonstersFactoryConfig]
      timeTaken        <- serveTimeTaken.flatMap(IntAbove0.fromIntZIO)
      scarePerformance <- results.map(_.sum)
      scareVolume_Int <- {
        if (scarePerformance >= childScreamChance)
          Random.nextIntBetween(childScreamIntensity.low.value, childScreamIntensity.high.value)
        else ZIO.succeed(0)
      }
      scareVolume <- ZIO.fromEither(refineV[GreaterEqual[0]](scareVolume_Int))
      report = ScareReport(door.id, monster.id, timeTaken, scareVolume)
      _ <- Console.printLine(s"Scare Report: ${report.toJson}").mapError(t => t.getMessage).when(appConfig.printScareReports)
    } yield report).mapError(str => PowerPlantEncodingError(str))
  }

  private def buildDoorQueue(timezone: Timezone): ZIO[ClientDeps, PowerPlantError, RingBuffer[Int]] =
    (for {
      doorIds <- DoorApiClient.getDoorSet(timezone)
      rb = RingBuffer[Int](doorIds.length)
      _  = doorIds.foreach(rb.offer)
    } yield rb).mapError(err => PowerPlantDoorQueueError(s"Error while retrieving Doors: $err"))

  private def fileReports(reports: Seq[ScareReport]): ZIO[DataSource, PowerPlantError, Seq[Unit]] =
    ZIO
      .foreachPar(reports)(ScareReportRepository.storeReport)
      .mapError(err => PowerPlantFilingError(s"Error while filing ScareReports: $err"))

  private val factoryFloor: ZIO[Console with Clock with Random with ClientDeps, PowerPlantError, Seq[ScareReport]] = {
    val timezones: Seq[Int] = Range.inclusive(Timezone.MIN, Timezone.MAX)
    ZIO
      .foreach(timezones) { tz_int =>
        for {
          timezone        <- Timezone.fromZIO(tz_int).mapError(PowerPlantEncodingError)
          _               <- Console.printLine(s"Beginning work in Timezone $timezone").mapError(t => PowerPlantConsoleError(t.getMessage))
          doorQueue       <- buildDoorQueue(timezone)
          monsterIds_Ints <- MonsterApiClient.getAllMonsters.mapError(err => PowerPlantMonsterApiError(s"Error while retrieving Monster IDs: $err"))
          monsterIds      <- ZIO.foreachPar(monsterIds_Ints)(IntAbove0.fromIntZIO).mapError(PowerPlantEncodingError)
          monsters        <- ZIO.foreachPar(monsterIds)(MonsterApiClient.getMonster).mapError(err => PowerPlantMonsterApiError(s"Error while retrieving Monsters: $err"))
          result          <- ZIO.foreachPar(monsters)(monster => work(monster, doorQueue))
        } yield result.flatten
      }
      .map(_.flatten)
  }

  private def prepareFinalReportMessage(reports: Seq[ScareReport]): ZIO[ClientDeps, PowerPlantError, String] = {
    val totalScareVolume     = reports.map(_.scareVolume.value).sum
    val totalAttempts        = reports.length
    val successfulAttempts   = reports.count(_.scareVolume.value > 0)
    val unsuccessfulAttempts = reports.count(_.scareVolume.value == 0)
    val monstersByScareOutput = reports
      .groupBy(_.monsterId)
      .map {
        case (monsterId, scareReports) =>
          (monsterId, scareReports.map(_.scareVolume.value).sum)
      }
      .toSeq
      .sortBy(_._2)

    (for {
      worst3Monsters <-
        ZIO
          .foreachPar(monstersByScareOutput.take(3)) {
            case (monsterId, scareOutput) =>
              MonsterApiClient.getMonster(monsterId).map(m => m.name + s"(ID: $monsterId)" + s" $scareOutput ScareUnits")
          }
          .map(_.mkString(", "))
      top3Monsters <-
        ZIO
          .foreachPar(monstersByScareOutput.takeRight(3)) {
            case (monsterId, scareOutput) =>
              MonsterApiClient.getMonster(monsterId).map(m => m.name + s" (ID: $monsterId)" + s" - $scareOutput ScareUnits")
          }
          .map(_.mkString(", "))
    } yield {
      s"""
         $tableLine
         ${addRow("Total Scare Volume", totalScareVolume.toString)}
         ${addRow("Total Attempts", totalAttempts.toString)}
         ${addRow("Successful Attempts", successfulAttempts.toString)}
         ${addRow("Unsuccessful Attempts", unsuccessfulAttempts.toString)}
         ${addRow("Top 3 Monsters", top3Monsters)}
         ${addRow("Worst 3 Monsters", worst3Monsters)}"""
    }).mapError(err => PowerPlantFilingError(err.msg))
  }

  val width = 100
  private val tableLine = {
    "|" + (for (_ <- 0 to width) yield "-").mkString
  }

  private def addRow(title: String, contents: String) = {
    val extra   = (width / 2) - title.length
    val spaces  = (for (_ <- 0 to extra) yield " ").mkString
    val rowBody = s" $title$spaces | $contents"
    s"""|$rowBody
       |$tableLine"""
  }

  private val printFinalReport: ZIO[Console with ClientDeps with DataSource, PowerPlantError, Unit] =
    for {
      reports   <- ScareReportRepository.retrieveAllReports.mapError(err => PowerPlantFilingError(err.msg))
      reportMsg <- prepareFinalReportMessage(reports)
      _         <- Console.printLine(reportMsg).mapError(t => PowerPlantConsoleError(t.getMessage))
    } yield ()

  private val announceStartOfWork: ZIO[Console, PowerPlantError, Unit] =
    Console
      .printLine("***** Starting work day at the Monster Factory *****")
      .mapError(t => PowerPlantAnnouncementError(s"Error while announcing the start of the work day: ${t.getMessage}"))

  private val announceClosingTime: ZIO[Console, PowerPlantError, Unit] =
    Console
      .printLine("***** Closing time. Everybody go home *****")
      .mapError(t => PowerPlantAnnouncementError(s"Error while announcing the end of the work day: ${t.getMessage}"))

  private val confirmFactoryFloorIsReady: ZIO[ClientDeps, PowerPlantError, Boolean] =
    for {
      doorApiIsReady    <- DoorApiClient.apiIsReady.mapError(err => PowerPlantDoorApiError(err.msg))
      monsterApiIsReady <- MonsterApiClient.apiIsReady.mapError(err => PowerPlantMonsterApiError(err.msg))
    } yield doorApiIsReady && monsterApiIsReady

  private def waitForFactoryToBeReady(numTimesToWait: Int, delay: Duration): ZIO[ClientDeps with Clock, PowerPlantError, Unit] =
    for {
      _       <- ZIO.sleep(delay)
      isReady <- confirmFactoryFloorIsReady
    } yield {
      if (!isReady && numTimesToWait == 0) ZIO.fail(PowerPlantStartupError("Factory floor was not ready in time"))
      else if (!isReady && numTimesToWait > 0) waitForFactoryToBeReady(numTimesToWait - 1, delay)
      else if (isReady) ZIO.succeed(())
      else ZIO.fail(PowerPlantStartupError("Factory floor did not startup correctly"))
    }

  val run: ZIO[Console with DataSource with Random with Clock with ClientDeps, PowerPlantError, Unit] = {
    for {
      appConfig <- ZIO.service[MonstersFactoryConfig]
      _         <- waitForFactoryToBeReady(appConfig.numTimesToWaitForStartup.value, appConfig.startupDelay)
      _         <- announceStartOfWork
      startTime <- Clock.currentTime(TimeUnit.SECONDS)
      reports   <- factoryFloor
      _         <- fileReports(reports)
      _         <- printFinalReport
      endTime   <- Clock.currentTime(TimeUnit.SECONDS)
      _         <- announceClosingTime
      _         <- Console.printLine(s"Factory was running for ${endTime - startTime} seconds").mapError(t => PowerPlantConsoleError(t.getMessage))
    } yield ()
  }
}
