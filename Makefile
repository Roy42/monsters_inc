
build:
	sbt clean
	sbt compile

test: build
	sbt test

buildDocker: build
	sbt docker:publishLocal

startFactory:
	docker-compose -f docker/docker-compose.yml up -d
	docker logs -f monsters-factory

stopFactory:
	docker-compose -f docker/docker-compose.yml down