package com.danielroy.monsters_inc.doorApi

import com.danielroy.monsters_inc.database.DoorRepository
import com.danielroy.monsters_inc.{Child, Door, Fear}
import com.danielroy.monsters_inc.database.DoorRepositorySpec.testChild
import com.danielroy.monsters_inc.doorApi.Main.unsafeRun
import io.getquill.context.ZioJdbc.DataSourceLayer
import zhttp.http.{Method, Request, URL}
import zio.{ZEnv, ZIO, ZLayer}
import zio.test._

import javax.sql.DataSource

// TODO: Replace with mocked DataSource
object RoutesTest extends DefaultRunnableSpec {

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val testChild =
    Child.makeZIO(1, "Daniel Roy", Seq(Fear.Darkness), 4, 5, (3, 9))
  private val testDoor =
    testChild.flatMap(child => Door.makeZIO(1, (45.0000, 90.0000), 1, child))
  private val prepTestData: ZIO[DataSource, Object, Unit] =
    testDoor.flatMap(DoorRepository.storeDoor)

  override def spec: ZSpec[TestEnvironment, Any] = {

    // Initialise
    unsafeRun(prepTestData.provideCustomLayer(DataSource))

    suite("RoutesTest - Smoke test")(
      test("get doors for timezone") {
        val url = URL(URL.root.path / "doorSet" / "1")
        for {
          request <- Request.make(method = Method.GET, url = url, remoteAddress = None)
          response <- Routes.routes(request)
        } yield {
          assertTrue(response.status.code == 200)
        }
      },
      test("get door by ID") {
        val url = URL(URL.root.path / "door" / "1")
        for {
          request <- Request.make(method = Method.GET, url = url, remoteAddress = None)
          response <- Routes.routes(request)
        } yield {
          assertTrue(response.status.code == 200)
        }
      }
    ).provideCustomLayer(DataSource)
  }
}
