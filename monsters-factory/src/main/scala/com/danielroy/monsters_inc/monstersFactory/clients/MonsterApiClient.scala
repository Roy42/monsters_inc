package com.danielroy.monsters_inc.monstersFactory.clients

import com.danielroy.dnd_engine.Ints.IntAbove0
import com.danielroy.monsters_inc.Monster
import com.danielroy.monsters_inc.monstersFactory.MonstersFactoryConfig
import zhttp.http.Status
import zhttp.service.Client
import zio.ZIO
import zio.json.DecoderOps

object MonsterApiClient {

  val getAllMonsters: ZIO[ClientDeps, ClientError, Seq[Int]] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.monsterApiUrl.value + "/all/"
      response <- Client.request(url).mapError(t => MonsterApiClientError(t.getMessage))
      body <- response.bodyAsString.mapError(t => ParsingError(t.getMessage))
      ids <- ZIO.fromEither(body.fromJson[Seq[Int]]).mapError(s => ParsingError(s))
    } yield ids

  def getMonster(id: IntAbove0): ZIO[ClientDeps, ClientError, Monster] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.monsterApiUrl.value + "/monster/" + id.value
      response <- Client.request(url).mapError(t => MonsterApiClientError(t.getMessage))
      body <- response.bodyAsString.mapError(t => ParsingError(t.getMessage))
      monster <- ZIO.fromEither(body.fromJson[Monster]).mapError(s => ParsingError(s))
    } yield monster

  val apiIsReady: ZIO[ClientDeps, ClientError, Boolean] =
    for {
      config <- ZIO.service[MonstersFactoryConfig]
      url = config.monsterApiUrl.value + "/ready"
      response <- Client.request(url).mapError(t => DoorApiClientError(t.getMessage))
    } yield response.status == Status.OK
}
