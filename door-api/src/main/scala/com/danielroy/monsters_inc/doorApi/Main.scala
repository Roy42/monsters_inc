package com.danielroy.monsters_inc.doorApi

import com.danielroy.monsters_inc.database.DoorRepository
import com.danielroy.monsters_inc.generator.DoorGenerator
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.refineV
import io.getquill.context.ZioJdbc.DataSourceLayer
import zhttp.service._
import zhttp.service.server.ServerChannelFactory
import zio.config.ReadError
import zio.config.typesafe.TypesafeConfig
import zio.{System, _}

import javax.sql.DataSource

object Main extends App {

  private val makeConfigLayer =
    System
    .env("DOOR_API_CONF")
    .someOrFail("Please specify the Door API config file in the system environment variable DOOR_API_CONF")
      .foldZIO(
        _ => ZIO.fail(new IllegalArgumentException("Did not find the config file")),
        { configFilePath =>
          val file = new java.io.File(configFilePath)
          if (file.isFile) {
            ZIO.attempt(TypesafeConfig.fromHoconFile(file, DoorApiConfig.configDescriptor))
          } else {
            ZIO.fail {
              val filesInDir = file.getParentFile.listFiles().map(_.getName).mkString(",")
              new IllegalArgumentException(s"Did not find the config file: $configFilePath. Did you mean any of these? $filesInDir")
            }
          }
        }
      )

  private lazy val DataSource: ZLayer[Any, Nothing, DataSource] =
    DataSourceLayer.fromPrefix("myH2DB").orDie

  private val makeServer =
    for {
      appConfig <- ZIO.service[DoorApiConfig]
      server =
        Server.port(appConfig.port.value) ++
          Server.paranoidLeakDetection ++
          Server.app(Routes.routes)
    } yield server

  private val makeServerChannelFactory =
    for {
      config <- ZIO.service[DoorApiConfig]
    } yield EventLoopGroup.auto(config.threads.value) ++ ServerChannelFactory.auto

  // TODO: replace generateDoor line with ZStream.range
  private val generateRandomDoors: ZIO[DataSource with Random with DoorApiConfig, Object, Unit] =
    for {
      appConfig <- ZIO.service[DoorApiConfig]
      startingId <- ZIO.fromEither(refineV[Positive](1))
      doorsToSpawn <- ZIO.fromEither(refineV[Positive](appConfig.doorsToSpawn.value))
      doors     <- DoorGenerator.generate(startingId, doorsToSpawn)
      res       <- ZIO.foreachPar(doors)(DoorRepository.storeDoor)
    } yield ()

  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    // First unsafe run to collect the configuration
    val configLayer: ZLayer[Any, Nothing, DoorApiConfig] = unsafeRun(makeConfigLayer).orDie

    val server: ZIO[DoorApiConfig, Any, URIO[ZEnv, ExitCode]] = for {
      appConfig   <- ZIO.service[DoorApiConfig]
      serverChannelFactory <- makeServerChannelFactory
      server      <- makeServer
      _           <- generateRandomDoors
        .provideLayer(
          DataSource ++
            Random.live ++
            configLayer)
    } yield server.make
      .use(_ =>
        Console.printLine(s"Door API started at ${appConfig.host}:${appConfig.port}")
          *> Console.printLine(s"With configuration: $appConfig")
          *> ZIO.never
      )
      .provideCustomLayer(serverChannelFactory ++ DataSource)
      .exitCode

    // Second and final unsafe run to boot up the API server
    unsafeRun(
      server.provideLayer(
          configLayer
      )
    )
  }
}
