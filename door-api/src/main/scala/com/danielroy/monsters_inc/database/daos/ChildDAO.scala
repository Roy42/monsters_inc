package com.danielroy.monsters_inc.database.daos

import com.danielroy.monsters_inc.{Child, Fear}
import io.getquill.Embedded
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}
import zio.{IO, Task, ZIO}

case class ChildDAO(
    childId: Int,
    name: String,
    fears: String,
    heavySleeper: Int,
    screamChance: Int,
    screamIntensityLow: Int,
    screamIntensityHigh: Int,
    lastScared: String
) extends Embedded

object ChildDAO {

  implicit val childDaoEncoder: JsonEncoder[ChildDAO] = DeriveJsonEncoder.gen
  implicit val childDaoDecoder: JsonDecoder[ChildDAO] = DeriveJsonDecoder.gen

  def childToDao(child: Child): Task[ChildDAO] = {
    ZIO.attempt {
      ChildDAO(
        childId = child.id.value,
        name = child.name.value,
        fears = child.fears.value.map(Fear.toInt).mkString(","),
        heavySleeper = child.heavySleeper.value,
        screamChance = child.screamChance.value,
        screamIntensityLow = child.screamIntensity.low.value,
        screamIntensityHigh = child.screamIntensity.high.value,
        lastScared = child.lastScared.map(_.toString).getOrElse("")
      )
    }
  }

  def daoToChild(dao: ChildDAO): IO[String, Child] =
    Child.makeZIO(
      dao.childId,
      dao.name,
      dao.fears.split(",").map(s => Fear.fromInt(s.toInt)),
      dao.heavySleeper,
      dao.screamChance,
      (dao.screamIntensityLow, dao.screamIntensityHigh),
      dao.lastScared match {
        case str if str.isEmpty => None
        case str                => Option(str)
      }
    )

}
