package com.danielroy.dnd_engine

import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MinSize
import eu.timepit.refined.refineV
import zio.{IO, ZIO}

object Collections {
  type AtLeastOne[T] = Iterable[T] Refined MinSize[1]
  object AtLeastOne {
    def fromZIO[T](set: Iterable[T]): IO[String, AtLeastOne[T]] =
      ZIO.fromEither {
        if (set.nonEmpty) refineV[MinSize[1]](set)
        else Left("Set must contain at least 1 element")
      }
  }
}
