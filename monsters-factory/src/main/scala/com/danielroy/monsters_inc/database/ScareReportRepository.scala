package com.danielroy.monsters_inc.database

import com.danielroy.monsters_inc.ScareReport
import com.danielroy.monsters_inc.database.daos.ScareReportDAO
import io.getquill.{H2ZioJdbcContext, Literal}
import zio.{IO, ZIO}

import javax.sql.DataSource

object ScareReportRepository {

  val ctx = new H2ZioJdbcContext(Literal)

  import ctx._

  sealed trait ScareReportRepositoryError {
    val msg: String
  }
  case class ScareReportSavingError(msg: String) extends ScareReportRepositoryError
  case class ScareReportEncodingError(msg: String) extends ScareReportRepositoryError
  case class ScareReportDecodingError(msg: String) extends ScareReportRepositoryError
  case class ScareReportRetrievalError(msg: String) extends ScareReportRepositoryError

  def storeReport(report: ScareReport): ZIO[DataSource, ScareReportRepositoryError, Unit] =
    for {
      dao <- ScareReportDAO.scareReportToDAO(report).mapError(t => ScareReportEncodingError(t.getMessage))
      success <- ctx.run(
        quote {
          query[ScareReportDAO].insertValue(lift(dao))
        }
      ).mapError(sqlException => ScareReportSavingError(sqlException.getMessage))
      _ <- IO.fail(ScareReportSavingError(s"Failed to save scare report: $report")).when(success == 0L)
    } yield ()

  val retrieveAllReports: ZIO[DataSource, ScareReportRepositoryError, Seq[ScareReport]] =
    for {
      daos <- ctx.run(
        query[ScareReportDAO]
      ).mapError(err => ScareReportRetrievalError(err.getMessage))
      reports <- daosToScareReports(daos)
    } yield reports

  private def daosToScareReports(daos: Seq[ScareReportDAO]): IO[ScareReportDecodingError, Seq[ScareReport]] =
    ZIO
      .foreach(daos) { dao =>
        ScareReport.makeZIO(
          dao.doorId,
          dao.monsterId,
          dao.timeTaken,
          dao.scareVolume
        )
      }.mapError(ScareReportDecodingError)
}

