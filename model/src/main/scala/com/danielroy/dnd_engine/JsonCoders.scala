package com.danielroy.dnd_engine

import com.danielroy.dnd_engine.Ints.{Int10Range, Int10Value, Int20Value, IntAbove0}
import com.github.nscala_time.time.Imports.DateTime
import eu.timepit.refined
import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.numeric.Interval.Closed
import eu.timepit.refined.refineV
import eu.timepit.refined.types.string.NonEmptyString
import zio.json.{JsonDecoder, JsonEncoder, JsonFieldDecoder, JsonFieldEncoder}

import scala.util.Try

object JsonCoders {
  // TODO: check if zio-json-interop-refined has been released
  // https://zio.github.io/zio-json/docs/interop/interop_refined
  // Code below copied from:
  // https://github.com/zio/zio-json/blob/develop/zio-json-interop-refined/shared/src/main/scala/zio/json/interop/refined/package.scala
  implicit def encodeRefined[A: JsonEncoder, B]: JsonEncoder[A Refined B] =
    JsonEncoder[A].contramap(_.value)
  implicit def decodeRefined[A: JsonDecoder, P](implicit V: Validate[A, P]): JsonDecoder[A Refined P] =
    JsonDecoder[A].mapOrFail(refineV[P](_))
  implicit def encodeFieldRefined[A: JsonFieldEncoder, B]: JsonFieldEncoder[A Refined B] =
    JsonFieldEncoder[A].contramap(_.value)
  implicit def decodeFieldRefined[A: JsonFieldDecoder, P](implicit V: Validate[A, P]): JsonFieldDecoder[A Refined P] =
    JsonFieldDecoder[A].mapOrFail(refineV[P](_))

  // Refined NonEmptyString encoder and decoder
  implicit val encoderNonEmptyString: JsonEncoder[NonEmptyString] =
    JsonEncoder[String].contramap(_.value)
  implicit val decoderNonEmptyString: JsonDecoder[NonEmptyString] =
    JsonDecoder[String].mapOrFail(NonEmptyString.from)

  implicit val datetimeEncoder: JsonEncoder[DateTime] =
    JsonEncoder[String].contramap(_.toString)
  implicit val datetimeDecoder: JsonDecoder[DateTime] =
    JsonDecoder[String].map(DateTime.parse)

  implicit val encoderIntAbove0: JsonEncoder[IntAbove0] =
    JsonEncoder[String].contramap(_.value.toString)
  implicit val decoderIntAbove0: JsonDecoder[IntAbove0] =
    JsonDecoder[Int].mapOrFail(IntAbove0.from)

  implicit val encoderInt10Value: JsonEncoder[Int10Value] =
    JsonEncoder[Int].contramap[Int10Value](_.value)
  implicit val decoderInt10Value: JsonDecoder[Int10Value] =
    JsonDecoder[Int].mapOrFail(refined.refineV[Closed[1, 10]](_))

  implicit val encoderInt20Value: JsonEncoder[Int20Value] =
    JsonEncoder[Int].contramap[Int20Value](_.value)
  implicit val decoderInt20Value: JsonDecoder[Int20Value] =
    JsonDecoder[Int].mapOrFail(refined.refineV[Closed[1, 20]](_))

  implicit val encoderInt10Range: JsonEncoder[Int10Range] =
    JsonEncoder[String].contramap { range =>
      s"(${range.low},${range.high})"
    }
  implicit val decoderInt10Range: JsonDecoder[Int10Range] =
    JsonDecoder[String].mapOrFail { str =>
      val cleaned = str.trim.replace("(", "").replace(")", "")
      for {
        a <- Try(cleaned.split(",", 2).map(_.toInt)).toEither.left.map(_.getMessage)
        l = a.head
        h = a.tail.head
        low   <- Int10Value.from(l)
        high  <- Int10Value.from(h)
        range <- Int10Range.make(low, high)
      } yield range
    }
}
