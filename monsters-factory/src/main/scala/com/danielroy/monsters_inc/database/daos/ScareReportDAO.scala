package com.danielroy.monsters_inc.database.daos

import com.danielroy.monsters_inc.ScareReport
import zio.{Task, ZIO}

case class ScareReportDAO(doorId: Int, monsterId: Int, timeTaken: Int, scareVolume: Int)

object ScareReportDAO {

  def scareReportToDAO(report: ScareReport): Task[ScareReportDAO] = ZIO.attempt {
    ScareReportDAO(
      report.doorId.value,
      report.monsterId.value,
      report.timeTaken.value,
      report.scareVolume.value
    )
  }
}