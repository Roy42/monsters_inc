package com.danielroy.monsters_inc.doorApi

import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Greater
import eu.timepit.refined.types.string.NonEmptyString

case class DoorApiConfig(
    host: NonEmptyString,
    port: Int Refined Greater[8000],
    threads: Int Refined Greater[0],
    doorsToSpawn: Int Refined Greater[500]
) {
  override def toString: String =
    s"DoorApiConfig(host: ${host.value}, port: ${port.value}, threads: ${threads.value}, doorsToSpawn: ${doorsToSpawn.value})"
}

object DoorApiConfig {

  import zio.config._
  import zio.config.refined._ // IntelliJ marks this as unused but it actually is
  import zio.config.magnolia.DeriveConfigDescriptor.descriptor

  val configDescriptor: ConfigDescriptor[DoorApiConfig] =
    descriptor[DoorApiConfig]
      .mapKey(toKebabCase)
      .mapKey(_.toLowerCase)

}
