CREATE TABLE IF NOT EXISTS ScareReportDAO(
    reportId int NOT NULL AUTO_INCREMENT,
    doorId int NOT NULL,
    monsterId int NOT NULL,
    timeTaken int NOT NULL,
    scareVolume int NOT NULL,
    PRIMARY KEY (reportId)
)