package com.danielroy.monsters_inc.database.daos

import com.danielroy.monsters_inc.Door
import zio.Task
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class DoorDAO(doorId: Int, latitude: Double, longitude: Double, timezone: Int, child: ChildDAO)

object DoorDAO {

  def doorToDaoZIO(door: Door): Task[DoorDAO] =
    for {
      childDao <- ChildDAO.childToDao(door.child)
    } yield {
      DoorDAO(
        door.id.value,
        door.geoLocation.latitude.value,
        door.geoLocation.longitude.value,
        door.timezone.value,
        childDao
      )
    }

  implicit val doorDaoEncoder: JsonEncoder[DoorDAO] = DeriveJsonEncoder.gen
  implicit val doorDaoDecoder: JsonDecoder[DoorDAO] = DeriveJsonDecoder.gen
}
