package com.danielroy

import com.danielroy.dnd_engine.JsonCoders._

import eu.timepit.refined.api.{Refined, RefinedTypeOps}
import eu.timepit.refined.numeric.Interval.Closed
import eu.timepit.refined.types.string.NonEmptyString
import zio._
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

import scala.language.implicitConversions
import scala.util.Try

package object monsters_inc {

  sealed trait Fear
  object Fear {
    case object Darkness extends Fear
    case object Shadows extends Fear
    case object Spiders extends Fear
    case object Snakes extends Fear
    case object Clowns extends Fear
    case object Dogs extends Fear
    case object Storms extends Fear
    case object Needles extends Fear
    case object Germs extends Fear
    case object Blood extends Fear

    def fromInt: Int => Fear = {
      case 0 => Darkness
      case 1 => Shadows
      case 2 => Spiders
      case 3 => Snakes
      case 4 => Clowns
      case 5 => Dogs
      case 6 => Storms
      case 7 => Needles
      case 8 => Germs
      case 9 => Blood
    }

    def toInt: Fear => Int = {
      case Darkness => 0
      case Shadows => 1
      case Spiders => 2
      case Snakes => 3
      case Clowns => 4
      case Dogs => 5
      case Storms => 6
      case Needles => 7
      case Germs => 8
      case Blood => 9
    }
  }
  implicit val encoderFear: JsonEncoder[Fear] = DeriveJsonEncoder.gen[Fear]
  implicit val decoderFear: JsonDecoder[Fear] = DeriveJsonDecoder.gen[Fear]

  type NonEmptyName = NonEmptyString
  object NonEmptyName extends RefinedTypeOps[NonEmptyName, String] {
    def fromZIO(s: String): IO[String, NonEmptyName] = ZIO.fromEither(super.from(s))
  }

  type Latitude = Double Refined Closed[-90.0, 90.0]
  object Latitude extends RefinedTypeOps[Latitude, Double]
  implicit val encoderLatitude: JsonEncoder[Latitude] =
    JsonEncoder[Double].contramap(_.value)
  implicit val decoderLatitude: JsonDecoder[Latitude] =
    JsonDecoder[Double].mapOrFail(Latitude.from)
  type Longitude = Double Refined Closed[-180.0, 180.0]
  object Longitude extends RefinedTypeOps[Longitude, Double]
  implicit val encoderLongitude: JsonEncoder[Longitude] =
    JsonEncoder[Double].contramap(_.value)
  implicit val decoderLongitude: JsonDecoder[Longitude] =
    JsonDecoder[Double].mapOrFail(Longitude.from)

  case class GeoPoint private(latitude: Latitude, longitude: Longitude)
  object GeoPoint {
    def fromZIO(lat: Double, lon: Double): IO[String, GeoPoint] = ZIO.fromEither {
        for {
          latitude <- Latitude.from(lat)
          longitude <- Longitude.from(lon)
        } yield GeoPoint(latitude, longitude)
      }
  }
  implicit val encoderGeoPoint: JsonEncoder[GeoPoint] =
    JsonEncoder[String].contramap(gp => s"${gp.latitude},${gp.longitude}")
  implicit val decoderGeoPoint: JsonDecoder[GeoPoint] =
    JsonDecoder[String].mapOrFail { str =>
      val Array(latStr, lonStr) = str.split(",")
      for {
        latDouble <- Try(latStr.toDouble).toEither.left.map(_.getMessage)
        lonDouble <- Try(lonStr.toDouble).toEither.left.map(_.getMessage)
        latitude <- Latitude.from(latDouble)
        longitude <- Longitude.from(lonDouble)
      } yield GeoPoint(latitude, longitude)
    }

  type Timezone = Int Refined Closed[-12, 12]
  object Timezone extends RefinedTypeOps[Timezone, Int] {
    def fromZIO(t: Int): IO[String, Timezone] = ZIO.fromEither(super.from(t))

    final val MIN = -12
    final val MAX = 11
  }
  implicit val encoderTimezone: JsonEncoder[Timezone] =
    JsonEncoder[Int].contramap(_.value)
  implicit val decoderTimezone: JsonDecoder[Timezone] =
    JsonDecoder[Int].mapOrFail(Timezone.from)

  implicit val encoderFearStrength: JsonEncoder[FearStrength] =
    DeriveJsonEncoder.gen[FearStrength]
  implicit val decoderFearStrength: JsonDecoder[FearStrength] =
    DeriveJsonDecoder.gen[FearStrength]
}
