package com.danielroy.monsters_inc

import com.danielroy.dnd_engine.Collections._
import com.danielroy.dnd_engine.Ints._
import com.danielroy.dnd_engine.JsonCoders._
import com.github.nscala_time.time.Imports.DateTime
import eu.timepit.refined.types.string.NonEmptyString
import zio.IO
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class Child(id: IntAbove0, name: NonEmptyString, fears: AtLeastOne[Fear], heavySleeper: Int10Value, screamChance: Int20Value, screamIntensity: Int10Range, lastScared: Option[DateTime])

object Child {

  implicit val encoderChild: JsonEncoder[Child] = DeriveJsonEncoder.gen
  implicit val decoderChild: JsonDecoder[Child] = DeriveJsonDecoder.gen

  def makeZIO(id: Int, name: String, fears: Iterable[Fear], heavySleeper: Int, screamChance: Int, screamIntensity: (Int, Int), lastScared: Option[String] = None): IO[String, Child] = {
    for {
      id              <- IntAbove0.fromIntZIO(id)
      name            <- NonEmptyName.fromZIO(name)
      fears           <- AtLeastOne.fromZIO(fears)
      heavySleeper    <- Int10Value.fromZIO(heavySleeper)
      screamChance    <- Int20Value.fromZIO(screamChance)
      screamLow       <- Int10Value.fromZIO(screamIntensity._1)
      screamHigh      <- Int10Value.fromZIO(screamIntensity._2)
      screamIntensity <- Int10Range.makeZIO(screamLow, screamHigh)
    } yield Child(id, name, fears, heavySleeper, screamChance, screamIntensity, lastScared.map(DateTime.parse))
  }
}
