package com.danielroy.monsters_inc

import com.danielroy.dnd_engine.Collections.AtLeastOne
import com.danielroy.dnd_engine.Ints.{Int20Value, IntAbove0}
import com.danielroy.dnd_engine.JsonCoders._
import eu.timepit.refined.types.string.NonEmptyString
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}
import zio.IO

case class FearStrength(fear: Fear, potency: Int20Value)
case class Monster(id: IntAbove0, name: NonEmptyString, strengths: AtLeastOne[FearStrength], endurance: Int20Value, effectiveness: Int20Value)

object Monster {

  def makeFearStrengthZIO(fear: Fear, potency: Int): IO[String, FearStrength] =
    for {
      int20 <- Int20Value.fromZIO(potency)
    } yield FearStrength(fear, int20)

  def makeZIO(id: Int, name: String, strengths: Seq[FearStrength], endurance: Int, effectiveness: Int): IO[String, Monster] =
    for {
      id   <- IntAbove0.fromIntZIO(id)
      name <- NonEmptyName.fromZIO(name)
      strengthsSet <- AtLeastOne.fromZIO(strengths)
      endurance     <- Int20Value.fromZIO(endurance)
      effectiveness <- Int20Value.fromZIO(effectiveness)
    } yield Monster(id, name, strengthsSet, endurance, effectiveness)

  implicit val monsterEncoder: JsonEncoder[Monster] = DeriveJsonEncoder.gen
  implicit val monsterDecoder: JsonDecoder[Monster] = DeriveJsonDecoder.gen
}
